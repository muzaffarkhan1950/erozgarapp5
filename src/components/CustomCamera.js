import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useRef, useState } from "react";
import { Camera, CameraType } from "expo-camera";
import { Ionicons } from "@expo/vector-icons";

function CustomCamera({ onClosePressed, onPictureTaken }) {
  const [cameraType, setCameraType] = useState(CameraType.front);
  // this is needed if user has not given camera permission
  const [permission, requestPermission] = Camera.useCameraPermissions();
  requestPermission();

  const cameraRef = useRef();

  const onTakePicturePress = () => {
    cameraRef.current
      .takePictureAsync()
      .then((response) => {
        onPictureTaken(response.uri);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onCameraFlipPressed = () => {
    if (cameraType === CameraType.front) {
      setCameraType(CameraType.back);
    } else {
      setCameraType(CameraType.front);
    }
  };

  return (
    <View style={styles.mainCon}>
      <Camera style={styles.camera} ref={cameraRef} type={cameraType}>
        <View style={styles.headerIcon}>
          <TouchableOpacity onPress={onClosePressed}>
            <Ionicons name={"close"} color={"white"} size={50} />
          </TouchableOpacity>

          <TouchableOpacity onPress={onCameraFlipPressed}>
            <Ionicons name={"refresh"} color={"white"} size={50} />
          </TouchableOpacity>
        </View>
        <View style={styles.cameraCon}>
          <TouchableOpacity onPress={onTakePicturePress}>
            <Ionicons name={"camera"} color={"white"} size={50} />
          </TouchableOpacity>
        </View>
      </Camera>
    </View>
  );
}

const styles = StyleSheet.create({
  mainCon: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  cameraCon: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  headerIcon: {
    justifyContent: "space-between",
    flexDirection: "row",
  },
});

export { CustomCamera };
