import { StyleSheet, TouchableOpacity, View, Image } from "react-native";
import { Camera, CameraType } from "expo-camera";
import React, { useRef, useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import { CustomCamera } from "../components/CustomCamera";
import { CustomButton } from "../components/CustomButton";

export default function Profile({ route }) {
  const [showCamera, setShowCamera] = useState(false);
  const [pictureUri, setPictureUri] = useState("");

  const onCameraBtnPress = () => {
    if (showCamera === true) {
      setShowCamera(false);
    } else if (showCamera === false) {
      setShowCamera(true);
    }
  };

  return (
    <View style={{ flex: 1, marginTop: 40 }}>
      {showCamera === true ? (
        <CustomCamera
          onClosePressed={onCameraBtnPress}
          onPictureTaken={(picUri) => {
            // this will save the picture
            setPictureUri(picUri);
            // this will close the camera
            onCameraBtnPress();
          }}
        />
      ) : (
        <View />
      )}

      <CustomButton
        text={"cameraPlease"}
        bgColor={"red"}
        onBtnPress={onCameraBtnPress}
      />

      {pictureUri !== "" ? (
        <Image source={{ uri: pictureUri }} style={styles.img} />
      ) : (
        <View />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  cameraCon: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  img: {
    height: 200,
    width: 200,
  },
});
